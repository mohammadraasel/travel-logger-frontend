import './log-entry-form.scss'
import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { useForm } from 'react-hook-form'
import { api } from '../../utils/api'

const LogEntryFrom = ({ entryLocation: { latitude, longitude }, onClose }) => {
	const [loading, setLoading] = useState(false)
	const [error, setError] = useState('')
	const { register, handleSubmit, errors} = useForm()

	const onSubmitted = async (data) => {
		setLoading(true)
		if (!data || data.title === '' || !data.visitDate) {
			return
		}

		try {
			await api.createLogEntry({ ...data, latitude, longitude })
			setLoading(false)
			onClose()
		} catch (error) {
			setError(error.message)
			setLoading(false)
		}
	}
	return (
		<form className='log-entry-form' onSubmit={handleSubmit(onSubmitted)} autoComplete='false'>
			{
				error && <h3 className="error">{error}</h3>
			}
			<div className="form-group">
				<label htmlFor="title">Title</label>
				<input ref={register({ required: true })} type="text" name="title" id="title" />
				{
					errors.title && <span className="error-message">Title is required.</span>
				}
			</div>
			<div className="form-group">
				<label htmlFor="image">ImageURL</label>
				<input ref={register()} type="text" name="image" id="image" />
			</div>
			<div className="form-group">
				<label htmlFor="visitDate">Visit Date</label>
				<input ref={register({ required: true })} type="date" name="visitDate" id="visitDate" />
				{
					errors.visitDate && <span className="error-message">Visit Date is required.</span>
				}
			</div>
			<div className="form-group">
				<label htmlFor="comments">Comments</label>
				<textarea rows='3' ref={register()} type="text" name="comments" id="comments" />
			</div>
			<div className="form-group">
				<label htmlFor="description">Description</label>
				<textarea rows="3" ref={register()} type="text" name="description" id="description"/>
			</div>

			<button
				disabled={loading}
				className="submit-button"
				type="submit"
			>
				{
					loading? 'Creating...': 'Create Log Entry'
				}
			</button>
		</form>
	)
}

LogEntryFrom.propTypes = {
	entryLocation: PropTypes.object,
	onClose: PropTypes.func,
}

export default LogEntryFrom

