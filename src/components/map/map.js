import React, { useState, useEffect, Fragment } from 'react';
import ReactMapGL, { Marker, Popup } from 'react-map-gl';
import 'mapbox-gl/dist/mapbox-gl.css';
import './map.scss'
import LogEntryFrom from '../log-entry-form';
import { api } from '../../utils/api'


function Map() {
  const [viewport, setViewport] = useState({
    latitude: 23.7807777,
    longitude: 90.3492856,
    zoom: 6,
    doubleClickZoom: false
  });
  const [showPopup, setShowPopup] = useState({})
  const [entryLocation, setEntryLocation] = useState(null)
  const [logEntries, setLogEntries] = useState([])

  const showAddMarkerPopup = (event) => {
    const [longitude, latitude] = event.lngLat
    setEntryLocation({
      latitude,
      longitude
    })
  }
  const getEntries = async () => {
    const logs = await api.fetchAllLogEntries()
    setLogEntries(logs.entries)
  }

  useEffect(() => {
    getEntries()
  }, [])

  return (
    <ReactMapGL
      {...viewport}
      width="100%"
      height="100%"
      onViewportChange={setViewport}
      mapStyle='mapbox://styles/mapbox/dark-v10'
      mapboxApiAccessToken={process.env.REACT_APP_MAPBOX_ACCESS_TOKEN}
      onDblClick = {showAddMarkerPopup}
    >
      {
        logEntries.map(entry => (
          <Fragment key={entry._id}>
            <Marker
              latitude={entry.latitude}
              longitude={entry.longitude}
              offsetLeft={-12}
              offsetTop={-24}
            >
              <div className="marker" onClick={()=> setShowPopup({[entry._id]: true})}>
                <svg
                  viewBox="0 0 24 24"
                  width="24"
                  height="24"
                  stroke="currentColor"
                  strokeWidth="2"
                  fill="none"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  className="marker-pin yellow">
                  <path d="M21 10c0 7-9 13-9 13s-9-6-9-13a9 9 0 0 1 18 0z"></path>
                  <circle cx="12" cy="10" r="3"></circle>
                </svg>
              </div>
            </Marker>
            {
              showPopup[entry._id] &&
              <Popup
                offsetTop={4}
                offsetLeft={4}
                latitude={entry.latitude}
                longitude={entry.longitude}
                closeButton={true}
                closeOnClick={false}
                onClose={() => setShowPopup({})}
                anchor="top"
              >
                <div className="popup-container">
                  <h3>{entry.title}</h3>
                  {
                    entry.image && <img src={entry.image} alt={entry.title}/>
                  }
                  <p>{entry.comments}</p>
                  <small>Visited on: {new Date(entry.visitDate).toLocaleDateString()}</small>
                </div>
              </Popup>
            }
          </Fragment>
        ))
      }
      {
        entryLocation ? 
          <Fragment>
            <Marker
              latitude={entryLocation.latitude}
              longitude={entryLocation.longitude}
              offsetLeft={-12}
              offsetTop={-24}
            >
              <div className="marker">
                <svg
                  viewBox="0 0 24 24"
                  width="24"
                  height="24"
                  stroke="red"
                  strokeWidth="2"
                  fill="none"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  className="marker-pin red">
                  <path d="M21 10c0 7-9 13-9 13s-9-6-9-13a9 9 0 0 1 18 0z"></path>
                  <circle cx="12" cy="10" r="3"></circle>
                </svg>
              </div>
            </Marker>
            <Popup
              offsetTop={4}
              offsetLeft={4}
              latitude={entryLocation.latitude}
              longitude={entryLocation.longitude}
              closeButton={true}
              closeOnClick={false}
              onClose={() => setEntryLocation(null)}
              anchor="top"
            >
                <div className="popup-container">
                <LogEntryFrom
                  entryLocation={entryLocation}
                  onClose={() => {
                    setEntryLocation(null)
                    getEntries()
                  }}
                />
                </div>
              </Popup>
          </Fragment>: null
      }     
    </ReactMapGL>
  );
}

export default Map