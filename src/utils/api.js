const API_URL = 'http://localhost:5000'

export const api = {
	fetchAllLogEntries: async () => {
		const response = await fetch(`${API_URL}/api/logs`)
		return response.json()
	},
	createLogEntry: async (entry) => {
		const response = await fetch(`${API_URL}/api/logs`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(entry)
		})
		return response.json()
	}
}