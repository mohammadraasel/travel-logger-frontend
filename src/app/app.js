import React from 'react';
import Map from '../components/map';

const styles = {
  app: {
    height: '100vh',
    width: '100%',
    backgroundColor: 'black'

  }
}

function App() {
  
  return (
    <div style={styles.app}>
      <Map/>
    </div>
  );
}

export default App;
